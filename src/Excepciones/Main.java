package Excepciones;

public class Main {

    public static void main(String[] args) throws InvalidNumberException {

        try {
            OperacionesMatematicas operaciones = new OperacionesMatematicas(8, 5);

            System.out.println(operaciones.sumar());
            System.out.println(operaciones.restar());
            System.out.println(operaciones.multiplicar());
            System.out.println(operaciones.dividir());
            System.out.println(operaciones.modulo());
        } catch (ArithmeticException ae) {
            System.out.println("El segundo número debe ser diferente de 0");
        } catch (InvalidNumberException ine) {
            System.out.println("El número debe ser mayor o igual que 5");
        }


    }
}
