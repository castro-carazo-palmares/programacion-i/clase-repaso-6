package Excepciones;

public class OperacionesMatematicas {

    private int numero1;
    private int numero2;

    public OperacionesMatematicas(int numero1, int numero2) throws InvalidNumberException {
        if (numero1 < 5 || numero2 < 5) {
            throw new InvalidNumberException("El número debe ser mayor o igual que 5");
        }

        this.numero1 = numero1;
        this.numero2 = numero2;
    }

    public int getNumero1() {
        return numero1;
    }

    public void setNumero1(int numero1) {
        this.numero1 = numero1;
    }

    public int getNumero2() {
        return numero2;
    }

    public void setNumero2(int numero2) {
        this.numero2 = numero2;
    }

    public int sumar() {
        return numero1 + numero2;
    }

    public int restar() {
        return numero1 - numero2;
    }

    public int multiplicar() {
        return numero1 * numero2;
    }

    public double dividir() {
        if (numero2 == 0) {
            throw new ArithmeticException();
        }

        return (double) numero1 / numero2;
    }

    public double modulo() {
        if (numero2 == 0) {
            throw new ArithmeticException();
        }

        return (double) numero1 % numero2;
    }

    @Override
    public String toString() {
        return "OperacionesMatematicas{" +
                "numero1=" + numero1 +
                ", numero2=" + numero2 +
                '}';
    }
}
